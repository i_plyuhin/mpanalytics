FROM python:3.10
# Or any preferred Python version.
ADD main.py .
ADD requirements.txt .
RUN pip install -r requirements.txt
CMD ["python", "./main.py"]
