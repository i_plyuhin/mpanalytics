from typing import Dict, Callable, List, Tuple
import requests
import prometheus_client as prom
import time
import os


class Item:
    price: int
    id: str
    name: str

    def __init__(self, id: str, price: int, name: str):
        self.price = price
        self.id = id
        self.name = name


def query_wildberries(items_ids: List[str]):
    headers = {'Accept': '*/*',
               'Accept-Language': 'en-US,en;q=0.9,ru;q=0.8',
               'Connection': 'keep-alive',
               'DNT': '1',
               'Origin': 'https://www.wildberries.ru',
               'Referer': f'https://www.wildberries.ru/catalog/{items_ids[0]}/detail.aspx?targetUrl=GP',
               'Sec-Fetch-Dest': 'empty',
               'Sec-Fetch-Mode': 'cors',
               'Sec-Fetch-Site': 'cross-site'
               }
    url = "https://card.wb.ru/cards/detail?" \
          "spp=0" \
          "&regions=80,64,58,83,4,38,33,70,82,69,68,86,30,40,48,1,22,66,31" \
          "&pricemarginCoeff=1.0" \
          "&reg=0" \
          "&appType=1" \
          "&emp=0" \
          "&locale=ru" \
          "&lang=ru" \
          "&curr=rub" \
          "&couponsGeo=2,12,7,3,6,13,21" \
          "&dest=-1113276,-79379,-1104258,-5803327" \
          f"&nm={';'.join(items_ids)}"
    resp = requests.get(url, headers=headers)
    return resp.json()


def get_items(json_resp: Dict) -> List[Item]:
    l = []
    for e in json_resp["data"]["products"]:
        l.append(Item(e["id"], e["salePriceU"] / 100, e["name"]))
    return l


prom.REGISTRY.unregister(prom.PROCESS_COLLECTOR)
prom.REGISTRY.unregister(prom.PLATFORM_COLLECTOR)
prom.REGISTRY.unregister(prom.GC_COLLECTOR)

items_on_monitor = os.environ["ITEMS"].split(" ")
gauge = prom.Gauge('items_price', 'This is my gauge', labelnames=["item", "name"])
prom.start_http_server(8000)

while True:
    r = query_wildberries(items_on_monitor)
    items = get_items(r)
    for e in items:
        gauge.labels(item=e.id, name=e.name).set(e.price)
    time.sleep(60)





