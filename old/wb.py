import logging
import json
import requests as r
from pprint import pprint
from typing import Dict, Callable, List, Tuple
from collections import namedtuple

s = """
    curl 'https://card.wb.ru/cards/detail?spp=0&regions=80,64,58,83,4,38,33,70,82,69,68,86,30,40,48,1,22,66,31&pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=2,12,7,3,6,13,21&dest=-1113276,-79379,-1104258,-5803327&nm=90951798;' \
  -H 'Accept: */*' \
  -H 'Accept-Language: en-US,en;q=0.9,ru;q=0.8' \
  -H 'Connection: keep-alive' \
  -H 'DNT: 1' \
  -H 'Origin: https://www.wildberries.ru' \
  -H 'Referer: https://www.wildberries.ru/catalog/90951798/detail.aspx?targetUrl=GP' \
  -H 'Sec-Fetch-Dest: empty' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Site: cross-site' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36' \
  -H 'sec-ch-ua: "Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "Linux"' \
  --compressed
    """
DUMP_FILENAME = "old/dump.json"
LOG_FILENAME = "old/app.log"
logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    level=logging.INFO, handlers=[logging.FileHandler(LOG_FILENAME), logging.StreamHandler()])


def __restore_state() -> Dict[str, List[str]]:
    with open(DUMP_FILENAME, "r") as f:
        try:
            return json.load(f)
        except json.decoder.JSONDecodeError as e:
            logging.error("Broken dump.json")
            return {}


def __dump_state():
    with open(DUMP_FILENAME, 'w') as f:
        json.dump(items_ids_by_user, f)


items_ids_by_user = __restore_state()
old_prices = {u: {k: 0 for k in items_ids_by_user[u]} for u in items_ids_by_user.keys()}

Item_description = namedtuple("Item", 'id name price')
Item_change = namedtuple("Item", 'id name price, old_price')


def __query_wildberries(items_ids: List[str]) -> Dict:
    headers = {'Accept': '*/*',
               'Accept-Language': 'en-US,en;q=0.9,ru;q=0.8',
               'Connection': 'keep-alive',
               'DNT': '1',
               'Origin': 'https://www.wildberries.ru',
               'Referer': f'https://www.wildberries.ru/catalog/{items_ids[0]}/detail.aspx?targetUrl=GP',
               'Sec-Fetch-Dest': 'empty',
               'Sec-Fetch-Mode': 'cors',
               'Sec-Fetch-Site': 'cross-site'
               }
    url = "https://card.wb.ru/cards/detail?" \
          "spp=0" \
          "&regions=80,64,58,83,4,38,33,70,82,69,68,86,30,40,48,1,22,66,31" \
          "&pricemarginCoeff=1.0" \
          "&reg=0" \
          "&appType=1" \
          "&emp=0" \
          "&locale=ru" \
          "&lang=ru" \
          "&curr=rub" \
          "&couponsGeo=2,12,7,3,6,13,21" \
          "&dest=-1113276,-79379,-1104258,-5803327" \
          f"&nm={';'.join(items_ids)}"
    resp = r.get(url, headers=headers)
    return resp.json()


def get_wb_price_and_name(item):
    resp = __query_wildberries([item])
    item = __get_items(resp)[0]
    return __save_get_field(__get_sale_price, item), __save_get_field(__get_item_name, item)


def __get_items(resp: Dict) -> List:
    return resp["data"]["products"]


def __get_sale_price(resp: Dict) -> int:
    return int(resp["salePriceU"] / 100)


def __get_item_name(resp: Dict) -> str:
    return resp["name"]


def __get_id(resp: Dict) -> str:
    return str(resp["id"])


def __save_get_field(get_field: Callable, resp):
    try:
        return get_field(resp)
    except KeyError as e:
        logging.error(e)


def add_item(user_id, id: str) -> None:
    if id == "":
        return
    items = items_ids_by_user.get(user_id, [])
    if id not in items:
        items.append(id)
        items_ids_by_user[user_id] = items
        old_prices[id] = 0
        __dump_state()


def delete_item(user_id, id: str) -> None:
    if id == "":
        return
    items_ids_by_user[user_id].remove(id)
    old_prices.pop(id)
    __dump_state()


def describe_items(user: str) -> List[Item_description]:
    if user not in items_ids_by_user.keys():
        return []
    resp = __query_wildberries(items_ids_by_user[user])
    items = __save_get_field(__get_items, resp)

    descriptions = []
    for e in items:
        descriptions.append(__get_items_description(e))

    return descriptions


def __get_items_description(item) -> Item_description:
    id = __save_get_field(__get_id, item)
    price = __save_get_field(__get_sale_price, item)
    name = __save_get_field(__get_item_name, item)
    return Item_description(id, name, price)


def run_wb() -> Tuple[Dict[str, List[Item_change]], Dict[str, List[Item_change]]]:
    reduced_prices = {}
    increased_prices = {}
    logging.info(f"Querying WB, old_prices:{str(old_prices)}")
    for user, items_ids in items_ids_by_user.items():
        response = __query_wildberries(items_ids)
        items = __save_get_field(__get_items, response)
        for item in items:
            id, name, price = __get_items_description(item)
            old_price = old_prices[user][id]
            logging.info(id)
            logging.info(name)
            logging.info(price)
            if old_price > price:
                logging.info(f"Reduced price for item {id}")
                _ = reduced_prices.get(user, [])
                _.append(Item_change(id, name, price, old_price))
                reduced_prices[user] = _
            if old_price != 0 and old_price < price:
                logging.info(f"Increased price for item {id}")
                _ = increased_prices.get(user, [])
                _.append(Item_change(id, name, price, old_price))
                increased_prices[user] = _
            old_prices[user][id] = price
    return reduced_prices, increased_prices

if __name__ == '__main__':
    items_ids  = ["43157560"]
    print(__query_wildberries(items_ids))