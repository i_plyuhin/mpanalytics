import signal
import sys
import logging
import time

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram import Update, ReplyKeyboardRemove
from telegram.ext import CallbackContext
from wb import add_item, delete_item, get_wb_price_and_name, describe_items, run_wb

LOG_FILENAME = "app.log"
TOKEN = ""
updater = Updater(token=TOKEN, use_context=True)


def start(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=__get_user_id(update), text="Hello I'm cool WB bot.")


def add_item_command(update: Update, context: CallbackContext):
    id = "".join(context.args)
    if id == "" or len(id) > 30:
        context.bot.send_message(chat_id=__get_user_id(update), text=f"Wrong id {id}")
        return
    add_item(__get_user_id(update), id)
    context.bot.send_message(chat_id=__get_user_id(update), text=f"Added item with id {id}")


def delete_item_command(update: Update, context: CallbackContext):
    id = "".join(context.args)
    if id == "":
        context.bot.send_message(chat_id=__get_user_id(update), text=f"Wrong id {id}")
        return
    delete_item(__get_user_id(update), id)
    context.bot.send_message(chat_id=__get_user_id(update), text=f"Deleted item with id {id}")


def get_price_command(update: Update, context: CallbackContext):
    id = "".join(context.args)
    price, name = get_wb_price_and_name(id)
    context.bot.send_message(chat_id=__get_user_id(update), text=f"Current price for item '{name}' is {price}₽")


def describe_items_command(update: Update, context: CallbackContext):
    message = ""
    for e in describe_items(__get_user_id(update)):
        message += f"{e.id} {e.name} {e.price}₽\n"
    context.bot.send_message(chat_id=__get_user_id(update), text=f"Your observed items:\n" + message)


def bot_send_message(chat_id, message):
    updater.bot.send_message(chat_id=chat_id, text=message, reply_markup=ReplyKeyboardRemove())


def run_bot():
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('add_item', add_item_command))
    dispatcher.add_handler(CommandHandler('delete_item', delete_item_command))
    dispatcher.add_handler(CommandHandler('get_price', get_price_command))
    dispatcher.add_handler(CommandHandler('describe_items', describe_items_command))
    updater.start_polling()


def terminate(signal, frame):
    logging.info("Ctrl+C Terminating")
    updater.stop()
    sys.exit(0)


def __get_user_id(update: Update):
    return str(update.effective_chat.id)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        level=logging.INFO, handlers=[logging.FileHandler(LOG_FILENAME), logging.StreamHandler()])
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    signal.signal(signal.SIGINT, terminate)
    logging.info("-----------------------DESTROYY WILDBERRIES!!!!--------------------------")
    run_bot()
    while True:
        try:
            reduced_prices, increased_prices = run_wb()
            for user, items_changes in reduced_prices.items():
                for e in items_changes:
                    bot_send_message(user, f"❗Reduced❗ price for item {e.id} Name: {e.name}"
                                           f"\nOld price was {e.old_price}₽"
                                           f"\nCurrent price is {e.price}₽"
                                           f"\nPercent change -{100 - round(e.price * 100 / e.old_price, 2)}%")
            for user, items_changes in increased_prices.items():
                for e in items_changes:
                    bot_send_message(user, f"❗Increased❗ price for item {e.id} Name: {e.name}"
                                           f"\nOld price was {e.old_price}₽"
                                           f"\nCurrent price is {e.price}₽"
                                           f"\nPercent change {round(e.price * 100 / e.old_price, 2) - 100}%")
        except Exception as e:
            logging.error(e)
            pass
        time.sleep(300)
